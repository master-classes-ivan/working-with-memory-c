#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>

#if 1

// Глобальная
int a = 1;

void func(int b) {
    // Внутри функции
    printf("a %d\n", a);

    printf("b %d\n", b);

    int c = 1;
    printf("c %d\n", c);
}

int main() {
    // Внутри функции
    printf("a %d\n", a);

    int b = 1;

    for (int i = 0; i < 5; i++) {
        // Внутри цикла
        printf("a %d\n", a);
        printf("b %d\n", b);
        printf("i %d\n", i);
        {
            int c = 2;
            printf("с %d\n", c);

        }
//        printf("с %d\n", c);
    }

//    func(b);


    return 0;
}

#endif

#if 0

int* func() {
    int a = 1;
    return &a;
}


int main() {

    int* ans;
    ans = func();
    printf("Hello\n");

    printf("Val is %d", *ans);

    return 0;
}

#endif

#if 0

int main() {

    printf("%u int\n",                    sizeof(int));
    printf("%u unsigned int\n",           sizeof(unsigned int));
    printf("%u long int\n",               sizeof(long int));
    printf("%u unsigned long int\n",      sizeof(unsigned long int));
    printf("%u long long int\n",          sizeof(long long int));
    printf("%u unsigned long long int\n", sizeof(unsigned long long int));
    printf("%u char\n",                   sizeof(char));
    printf("%u unsigned char\n",          sizeof(unsigned char));
    printf("%u double\n",                 sizeof(double));

    printf("\n");

//    printf("%u int8_t\n",                 sizeof(int8_t));
//    printf("%u int16_t\n",                sizeof(int16_t));
//    printf("%u int32_t\n",                sizeof(int32_t));
//    printf("%u int64_t\n",                sizeof(int64_t));

    printf("\n");

//    printf("%u int*\n",                   sizeof(int*));
//    printf("%u double*\n",                sizeof(double*));
//    printf("%u char*\n",                  sizeof(char*));
//    printf("%u long long int*\n",         sizeof(long long int*));
//    printf("%u void*\n",                  sizeof(void*));

    return 0;
}

#endif

#if 0

int main() {
    int* ap = (int*) malloc(sizeof (int));

    printf("ap %d\n", *ap);
    *ap = 1;
    printf("ap %d\n", *ap);

    printf("\n");

//    int* bp = (int*) calloc(1, sizeof (int));

//    printf("bp %d\n", *bp);
//    *bp = 1;
//    printf("bp %d\n", *bp);

    printf("\n");

//    int* cp;
//    printf("cp %p\n", cp);
//    printf("cp %d\n", *cp);

    printf("Finish");

    return 0;
}

#endif

#if 0

int main() {
    int a[5] = {1,2,3,4,5};
//    for (int i = 0; i < 5; i++) {
//        printf("%d\n", a[i]);
//    }

//    printf("Addr is %p Val is %d\n", a, *a);
//    printf("Addr is %p Val is %d\n", a+1, *(a+1));
//    printf("Addr is %p Val is %d\n", a+2, *(a+2));
//    printf("Addr is %p Val is %d\n", a+3, *(a+3));

    printf("\n");

//    int* bp = a+1;
//    printf("Addr is %p Val is %d\n", bp, *bp);

//    int* c = (int*) malloc (sizeof(int) * 5);
//    for(int i = 0; i < 5; i++) {
//        *(c+i) = i;
//    }

//    for(int i = 0; i < 5; i++) {
//        printf("%d\n", *(c+i));
//    }

//    free(c);

    return 0;
}

#endif



#if 0

int main() {
    printf("Start\n");
//    int a[5000000];

    int* ap = (int*) malloc(sizeof(int) * 500000000);


    printf("Finish\n");

    free(ap);

    return 0;
}

#endif

#if 0

int main() {
    int a[5][5];
    for(int i = 0; i < 5; i++) {
        for(int j = 0; j < 5; j++) {
            a[i][j] = i+j;
        }
    }

    for(int i = 0; i < 5; i++) {
        for(int j = 0; j < 5; j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    printf("\n");
//    printf("%d", *a);

    for(int i = 0; i < 5; i++) {
        for(int j = 0; j < 5; j++) {
            printf("%d ", *((int*)a + (i*5) + j));
        }
        printf("\n");
    }
    return 0;
}

#endif

#if 0

void print_arr(int a[]) {
    for(int i = 0; i < 5; i++){
        printf("%d ", a[i]);
    }
}

//void print_arr_2(int* a) {
//    for(int i = 0; i < 5; i++){
//        printf("%d ", a[i]);
//    }
//}


//void print_arr_2d(int a[][5]) {
//    for(int i = 0; i < 5; i++){
//        for (int j = 0; j < 5; j++){
//            printf("%d ", a[i][j]);
//        }
//        printf("\n");
//    }
//}

//void print_arr_3d(int a[][5][5]);


int main() {
    int a[5] = {1,2,3,4,5};
    print_arr(a);
//    print_arr_2(a);

//    printf("\n\n");
//    int b[5][5] = {{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5},{1,2,3,4,5}};
//    print_arr_2d(b);
    return 0;
}

#endif

#if 0

int main() {
    int a = 5;
    int* b = &a;
    int** c = &b;

    printf("1 %d\n", a);
    **c = 6;
    printf("2 %d\n", a);
    return 0;
}

#endif

#if 0

int main() {
    int** a;

    a = (int**) malloc(sizeof (int*) * 5);

    for (int i = 0; i < 5; i++) {
        a[i] = (int*) malloc(sizeof (int) * 5);
    }

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            a[i][j] = j;
        }
    }

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            free(a[i]);
        }
    }

    free(a);


    return 0;
}

#endif



#if 0

int main() {
    int32_t* ap = (int32_t*) malloc(sizeof(int32_t) * 4);
    *ap = 1;
    *(ap+1) = 2;
    printf("addr 1 %p\n", ap);
    printf("addr 2 %p\n", ap+1);
    printf("addr 3 %p\n", ap+2);
    printf("addr 4 %p\n", ap+3);

    printf("\n");

    printf("addr 1 %p\n", (int8_t*)ap);
    printf("addr 2 %p\n", (int8_t*)ap + 1);
    printf("addr 3 %p\n", (int8_t*)ap + 2);
    printf("addr 4 %p\n", (int8_t*)ap + 3);


    printf("val %" PRId32 "\n\n", *ap);
    printf("val 1 %d\n", *((int8_t*)ap+0));
    printf("val 2 %d\n", *((int8_t*)ap+1));
    printf("val 3 %d\n", *((int8_t*)ap+2));
    printf("val 4 %d\n", *((int8_t*)ap+3));

    int8_t* bp = (int8_t*) ap;

    *(bp+0) = 1;
    *(bp+1) = 3;
    *(bp+2) = 7;
    *(bp+3) = 15;

    printf("\n\n");

    printf("val %d\n\n", *ap);
    printf("val 1 %d\n", *(bp+0));
    printf("val 2 %d\n", *(bp+1));
    printf("val 3 %d\n", *(bp+2));
    printf("val 4 %d\n", *(bp+3));




    return 0;
}

#endif



#if 0

int main() {
    printf("Hello World!\n");
    return 0;
}

#endif

#if 0

int main() {
    printf("Hello World!\n");
    return 0;
}

#endif

#if 0

int main() {
    printf("Hello World!\n");
    return 0;
}

#endif
